<?php session_start();
 //session_destroy();
if($_SESSION['is_loggedin'] == false){
  header("location:login.php");
}
include("config/config.php");
?>
<!DOCTYPE html>
<html>
<head>
<!-- Import Bootstrap from CDN-->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<!--Extra Theme-->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
<!--Import jQuary from CDN-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

<!-- Extra CSS -->
<style>
.text-right {
  float: right;
}
body {
  background: #16a085;
}
</style>
</head>
<body>
<div class="container">
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">API Users Area</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="#">Home</a></li>
      <li><a href="hier.php">Find Employee</a></li>
      
    </ul>
    <ul class="nav navbar-nav navbar-right">
      
      <li><a href="logout.php"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
    </ul>
  </div>
</nav>
  <div class="row">
    <div class="col-md-9">
      <div class="panel panel-primary"> 
	     <div class="panel-heading">Welcome <?php echo $_SESSION['loggedin_user'];?>!</div>
		 <div class="panel-body">

      <?php include("api/api_config.php"); ?>
		    <form class="form-horizontal" action="import_json.php" method="post" name="uploadCSV"  enctype="multipart/form-data">
          <div class="input-row">
              <label class="">Paste your JSON data</label> 
              <input type="file" name="file" id="file" accept=".csv">  
   
              
              <input type="hidden" name="loggedinUser" value="<?php echo $_SESSION['loggedin_user'];?>"> <br>
              <input type="hidden" name="api_username" value="<?php echo $API_USERNAME;?>">
              <input type="hidden" name="api_password" value="<?php echo $API_PASSWORD;?>">
              <input type="hidden" name="api_scope" value="<?php echo $API_SCOPE;?>">
              <button type="submit" id="submit" name="import" value="import" class="btn-submit">Import</button>
              <br />

          </div>
            <div id="msg" class="<?php echo (isset($_GET['msg']))?$_GET['type']:""?>"><br>
              <?php
                if(isset($_GET['msg']))
                  echo $_GET['msg'];
              ?>
            </div>
        </form>

        <?php
        
        ?>
		 </div>

      </div>
    </div>
	
  </div>

</div>
</body>
</html>

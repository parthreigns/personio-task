<?php 
include("config/config.php");

// get instances of API files
include("api/token/index.php");

// Set credentials of API

  $apiusername = $_POST['api_username'];
  $apipassword = $_POST['api_password'];
  $apiscope = $_POST['api_scope'];
      
  	if($apiscope == "grant_employee_info"){
    	$test = new GrantAccessAPI();	
		if(isset($_POST['api_username']) && isset($_POST['api_password'])){
			$token = $test->getAccess($_POST, $conn);
		}
		
		// check if token is success of not, if yes then only it will get the further function work.

		if ($token['status'] == "success") {

		    if(!empty($_FILES)){ //if import by CSV

			    $fileName = $_FILES["file"]["tmp_name"];
			    
			    if ($_FILES["file"]["size"] > 0) {
			        
			        $file = fopen($fileName, "r");
			        $row = 0;
			        while (($column = fgetcsv($file, 10000, ",")) !== FALSE) {
			        	if($row != 0){
			        		
			        		$sql = "select count(emp_unique_id) as cnt from employees where emp_unique_id = '" . $column[0] . "' and var_name = '" . $column[1] . "'";
			        		$resultSql = mysqli_query($conn, $sql);
			        		$getrows = mysqli_fetch_array($resultSql);
			        		if($getrows['cnt'] > 0){
			        			 $sqlUpdate = "update employees set supervisorID = '" . $column[2] . "' where  emp_unique_id = '" . $column[0] . "'";
				                   
				            	$result = mysqli_query($conn, $sqlUpdate);
			        		}else{
					            $sqlInsert = "INSERT into employees (emp_unique_id,var_name,supervisorID)
					                   values ('" . $column[0] . "','" . $column[1] . "','" . $column[2] . "')";
					            $result = mysqli_query($conn, $sqlInsert);	
				            }
				            if (! empty($result)) {
				                $type = "success";
				                $message = "CSV Data Imported into the Database";

				            } else {
				                $type = "error";
				                $message = "Problem in Importing CSV Data";
				            }
			        	}
			            $row++;
			        }
			    }
			}
			else {
				// if import by JSON data
				

				$jsond = json_decode($_POST['jsond']);
				foreach($jsond as $json){
					
					$sql = "select count(emp_unique_id) as cnt from employees where emp_unique_id = '" . $json->EmpId . "' and var_name = '" . $json->EmpName . "'";
	        		$resultSql = mysqli_query($conn, $sql);
	        		$getrows = mysqli_fetch_array($resultSql);
	        		if($getrows['cnt'] > 0){
	        			 $sqlUpdate = "update employees set supervisorID = '" . $json->SuprvisorID . "' where  emp_unique_id = '" . $json->EmpId . "'";
		                   
		            	$result = mysqli_query($conn, $sqlUpdate);
	        		}else{
			            $sqlInsert = "INSERT into employees (emp_unique_id,var_name,supervisorID)
			                   values ('" . $json->EmpId . "','" . $json->EmpName . "','" . $json->SuprvisorID . "')";
			            $result = mysqli_query($conn, $sqlInsert);	
		            }
				}
				
			}

			$sqlTree = "SELECT var_name, emp_unique_id, supervisorID FROM `employees` ";
            $resultSqlTree = mysqli_query($conn, $sqlTree);
            $num_rows = mysqli_num_rows($resultSqlTree);

            if($num_rows > 0){
              while($getrowsTree = mysqli_fetch_assoc($resultSqlTree)){
                  $empData[] = $getrowsTree;
              }

              $Emparr = buildTree($empData, 0);
              
              if(empty($Emparr))
                echo $_POST['var_name']." has no Supervisor. He/She should be at top level.";
              else{
                echo "<br> Imported employees data is showing below Hierarchy <br><br>";
                echo json_encode($Emparr);
              }
          }
          else{
            echo "No Employee has been found with provided name, Try again! <a href='javascript: window.history.go(-1)'>Back</a>";
          }
		}
		else{
			if(!empty($_FILES))
				header("location:import_csv.php?msg=API access Denied");
			else
				header("location:index.php?msg=API access Denied");
		}
	}

function buildTree(array $elements, $parentId = 0) {
                  $branch = array();

                  foreach ($elements as $element) {
                      if ($element['supervisorID'] == $parentId && $element['var_name']  != $_POST['var_name']) {
                          $children = buildTree($elements, $element['emp_unique_id']);
                          if ($children) {
                                  //prev($children);
                                       $element['supervises'] = $children;   
                               
                              
                          }
                          
                          unset($element['emp_unique_id']);
                          unset($element['supervisorID']);

                          $branch[] = $element;
                      }
                      
                  }

                  return $branch;
          }
        
            
        
	
//header("location:index.php?msg=".$message."&type=".$type);

?>
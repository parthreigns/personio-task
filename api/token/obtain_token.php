<?php
	function obtainToken($ApiUsername, $ApiPassword, $scope) {
	   // echo "Obtaining token...";

	    // prepare the request
	    $uri = 'http://localhost/personio-task/api/token/';


	    $token = base64_encode("$ApiUsername:$ApiPassword");
	    $accessCred = array(
	        'grant_type' => 'api_credentials',
	        'scope'      => $scope,
	        'api_username' => $ApiUsername,
	        'api_password' => $ApiPassword
	    );

	    // build the curl request
	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL, $uri);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch,CURLOPT_POST, true);
		curl_setopt($ch,CURLOPT_POSTFIELDS, http_build_query($accessCred));
		
	    
		
	    // process and return the response
	    $response = curl_exec($ch);
	    
	    
	    if (! isset($response['access_token'])
	        || ! isset($response['token_type'])) {
	        return $response['status'] = "failed";
	    }
	    curl_close($ch);
	    //echo "success!\n";
	    
	    // here's your token to use in API requests
	    return $response['token_type'] . " " . $response['access_token'];
	}
?>
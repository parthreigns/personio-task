<?php

include("config/config.php");

if(isset($_POST['var_name'])){
        
    // get instances of API files
	include("api/token/index.php");
	
	// Set credentials of API
	
	$apiusername = $_POST['api_username'];
	$apipassword = $_POST['api_password'];
	$apiscope = $_POST['api_scope'];
	
	if($apiscope == "grant_employee_info"){
		$test = new GrantAccessAPI();

		if(isset($_POST['api_username']) && isset($_POST['api_password'])){
			$token = $test->getAccess($_POST, $conn);
		}
		//$token = obtainToken($apiusername, $apipassword, $apiscope);
		
		// check if token is success of not, if yes then only it will get the further function work.
		if($token['status'] == "success"){


	        if(isset($_POST['var_name'])){
	            
	        
		        $sqlTree = "SELECT var_name, emp_unique_id, supervisorID FROM `employees` ";
		        $resultSqlTree = mysqli_query($conn, $sqlTree);
		        $num_rows = mysqli_num_rows($resultSqlTree);

		        if($num_rows > 0){
			        while($getrowsTree = mysqli_fetch_assoc($resultSqlTree)){
			            $empData[] = $getrowsTree;
			        }

			        $Emparr = buildTree($empData, 0);

			        if(empty($Emparr))
			        	echo $_POST['var_name']." has no Supervisor. He/She should be at top level.";
			        else
			        	echo json_encode($Emparr);
			    }
			    else{
			    	echo "No Employee has been found with provided name, Try again! <a href='javascript: window.history.go(-1)'>Back</a>";
			    }
		    }
		    else
		    {
		    	header("location:hier.php?msg=API Access failed");
		    }
		}
		else{
			header("location:hier.php?msg=API access Denied");
		}
	}
}

function buildTree(array $elements, $parentId = 0) {
        $branch = array();

        foreach ($elements as $element) {
            if ($element['supervisorID'] == $parentId && $element['var_name']  != $_POST['var_name']) {
                $children = buildTree($elements, $element['emp_unique_id']);
                if ($children) {
                        //prev($children);
                             $element['supervises'] = $children;   
                     
                    
                }
                
                unset($element['emp_unique_id']);
                unset($element['supervisorID']);

                $branch[] = $element;
            }
            
        }

        return $branch;
}

?>
<?php


include ("../config/config.php");

// create table on database
$create = "
CREATE TABLE `api_users` (
  `int_id` smallint(5) NOT NULL AUTO_INCREMENT,
  `var_username` varchar(30) NOT NULL,
  `var_pw` varchar(32) NOT NULL,
  `var_name` varchar(32) NOT NULL,
  PRIMARY KEY (int_id)
) 
ENGINE=InnoDB DEFAULT CHARSET=latin1;

";
$create1 = "
INSERT INTO `api_users` (`int_id`, `var_username`, `var_pw`, `var_name`) VALUES
(1, 'personia', '1234', 'Personia');

";
$create2 = "
CREATE TABLE `employees` (
  `int_empid` smallint(5) NOT NULL AUTO_INCREMENT,
  `emp_unique_id` int(11) DEFAULT NULL,
  `var_name` varchar(30) NOT NULL,
  `supervisorID` int(11) NOT NULL DEFAULT 0 COMMENT 'supervisorID',
  PRIMARY KEY (int_empid)
) 
ENGINE=InnoDB DEFAULT CHARSET=latin1;



";

mysqli_query($conn,$create) or die ("Could not create tables<br>" . mysqli_error($conn));
mysqli_query($conn,$create1) or die ("Could not create tables<br>" . mysqli_error($conn));
mysqli_query($conn,$create2) or die ("Could not create tables<br>" . mysqli_error($conn));

echo "Complete.";
?>

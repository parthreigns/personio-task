# Installation Instructions and Guide to use the application.
# I have used custom PHP to make this application and MySql as the relational Database. You will need WAMP/LAMP/XAMP to run this application.

1) Open configuration file config.php under /config/ folder and set your variables according to mysql database server.

2) Copy below URL in your browser to import initial Database Tables
	URL: http://localhost/personio-task/install/install.php

3) As mentioned, this API should be restrcited so you will have to first set up the configuration of API. Open the file api_config.php under /api/ folder. change variables accordingly to your credentials of API. For this App, i've used below credentials in the Database table "api_user": (it will be automatically created once installation is done as in step 2 above).

	username: personia
	password: 1234

4) Optional: To import/post the employees relationship data, create a CSV file (you can find sample CSV under /sample-csv/ folder). Use below URL to use it with GUI

	URL: http://localhost/personio-task/import_csv.php

5) Import JSON data: JSON string must be as below, because Name can be same for other employees so i have taken unique ID field and the same field instance i have used to make the relation between supervisor.
	e.g: SuprvisorID is the field which define the unique ID of other employee who is Supervises this employee.

	Optional: Use below URL to use it with GUI

	URL: http://localhost/personio-task/index.php

	To use by API directly:

	you will need to Post the below parameters of API:
	
	api_username: your API username
	api_password: your API password
	api_scope : must be "grant_employee_info"
	jsond: Must be a JSON formatted data, see below for proper format
	
	JSON Format:

	[
		{
			"EmpId":"1",
			"EmpName":"Pete",
			"SuprvisorID":3
		},
		{
			"EmpId":"2",
			"EmpName":"Barbara",
			"SuprvisorID":3
		},
		{
			"EmpId":"3",
			"EmpName":"Nick",
			"SuprvisorID":4
		},
		{
			"EmpId":"4",
			"EmpName":"Sophie",
			"SuprvisorID":5
		},
		{
			"EmpId":"5",
			"EmpName":"Jonas",
			"SuprvisorID":0
		}
	]

6) Optional: To find the employee, go to URL http://localhost/personio-task/hier.php and type employee name.
7) To use the API directly, use below URL
	e.g: http://localhost/personio-task/hier_action.php
	
	you will need to Post the below parameters of API:
	
	api_username: your API username
	api_password: your API password
	api_scope : must be "grant_employee_info"
	var_name: e.g Nick

8) Optional: if you want to work with GUI of this App, you can use below URL and login credentials:
	e.g: http://localhost/personio-task/

	login info: username is "personia" and password is "1234"


# Test Cases:

1) To find employee:
	e.g Nick
	
	If there will not be any employee found in the database, it will return the error message.
	If it will find the employee, it will retrieve the JSON formatted data with all possible supervisiors who supervises the employee "Nick".

2) To import JSON
	you can sample JSON data provided above in this README file.
	If it will be imported, then the data will be stored in the MySql database table "employees";
	If not, it will give you error message.